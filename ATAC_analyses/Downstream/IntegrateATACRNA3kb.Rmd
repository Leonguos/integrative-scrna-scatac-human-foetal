---
title: "Integrate scRNA and scATAC"
output: html_notebook
---

```{r}
library(Matrix)
library(tidyverse)
library(chromVAR)
set.seed(1978)
library(data.table)
library(Seurat)
library(Signac)
library(harmony)
library(BSgenome.Hsapiens.UCSC.hg38)
library(EnsDb.Hsapiens.v86)
library(pals)
library(ggrepel)
library(patchwork)
```

## Load scRNA object 
```{r}
load("/datadisk/Desktop/Projects/SCRNATAC/scRNA/ScanpyToSeurat-20200329T150351Z-001/ScanpyToSeurat/RNA_all_samples.robj")

```


## load ATAC data 

```{r}
test = readRDS("/datadisk/Desktop/Projects/SCRNATAC/scATAC/files/test.merged234.Snap.HA.ChromVar.Rds")

peakfile <- "/datadisk/Desktop/Projects/SCRNATAC/scATAC/peaks/snapatac.clust.peaks.bed"

peaks.df = as.data.frame(read_tsv(peakfile, col_names = F))
peaks.df$ID = paste0(peaks.df$X1,":",peaks.df$X2,"-",peaks.df$X3)
rownames(peaks.df) = peaks.df$ID

peakset = "consensus"
peaks <- getPeaks(peakfile, sort_peaks = TRUE)
```



```{r}
# size of extension upstream and downstream in kb 

extensions = c(3)
 # extract gene coordinates from Ensembl, and ensure name formatting is consistent with Seurat object 
gene.coords <- genes(EnsDb.Hsapiens.v86, filter = ~ gene_biotype == "protein_coding")
seqlevelsStyle(gene.coords) <- 'UCSC'
genebody.coords <- keepStandardChromosomes(gene.coords, pruning.mode = 'coarse')


for (ext in extensions) {
  
  
 
genebodyandpromoter.coords <- Extend(x = gene.coords, upstream = 1000*ext, downstream = 1000*ext)


if (file.exists(paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                                       "kdownup.merged2l234.rds"))) {
  
   gene.activities = readRDS(file = paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                                         "kdownup.merged2l234.rds"))
} else {
  gene.activities <- FeatureMatrix(
    fragments = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/fragments/merged.l234.sort.CO.bed.gz",
    features = genebodyandpromoter.coords,
    chunk = 20,
    cells = rownames(test@meta.data)
  )
  saveRDS(gene.activities, file = paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                                       "kdownup.merged2l234.rds"))


  gene.activities = readRDS(file = paste0("/datadisk/Desktop/Projects/SCRNATAC/scATAC/geneact/GENEact.",ext,
                                         "kdownup.merged2l234.rds"))

  
  }


 gene.key <- genebodyandpromoter.coords$gene_name
names(gene.key) <- GRangesToString(grange = genebodyandpromoter.coords)


rownames(gene.activities) <- gene.key[rownames(gene.activities)]

gene.activities = gene.activities[unique(rownames(gene.activities)),]

# add the gene activity matrix to the Seurat object as a new assay, and normalize it
test[[paste0("RNA_geneprom_",ext,"kb")]] <- CreateAssayObject(counts = gene.activities)
# test = BinarizeCounts(object = test, assay = c("RNA"))

test <- NormalizeData(
  object = test,
  assay = paste0("RNA_geneprom_",ext,"kb"),
  normalization.method = 'LogNormalize',
  scale.factor = median(test@meta.data[,paste0("nFeature_RNA_geneprom_",ext,"kb")])
)

rm(gene.activities)
  
  
}
```


```{r}

RNAcatsign = names(table(merged@meta.data[which(merged@meta.data$gate == "38-"),"annotation_merged"]))[table(merged@meta.data[which(merged@meta.data$gate == "38-"),"annotation_merged"]) > 20]


for (ext in extensions) {
  #  ext = 3
  transfer.anchors <- FindTransferAnchors(
    reference = subset(merged, subset = gate == "38-" & annotation_merged %in% RNAcatsign),
    query = test,
    reduction = 'cca',
    query.assay = paste0("RNA_geneprom_",ext,"kb"),
    features = rownames(test@assays[[paste0("RNA_geneprom_",ext,"kb")]]@counts),
    verbose = T
  )


  kweight = 6
    cutoff = 0.4 
    predicted.labels <- TransferData(
    anchorset = transfer.anchors,
    refdata = subset(merged, subset = gate == "38-" & annotation_merged %in% RNAcatsign)$annotation_merged,  
    weight.reduction = test[['harmony']],
    dims = 1:50,
    k.weight = kweight
    
  )
  
  
  datatable.test = data.table(predicted.labels, keep.rownames = T)


  cutoff = 0.4

  datatable.test$predicted.id = ifelse(datatable.test$prediction.score.max < cutoff, "notsign", datatable.test$predicted.id)

  datatable.test = as.data.frame(datatable.test)
  rownames(datatable.test) = datatable.test$rn
  
  test <- AddMetaData(object = test, metadata = as.data.frame(datatable.test))
}
    
```

```{r}
saveRDS(test, file = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/files/test.merged234.Snap.HA.ChromVar.Integr3kb.Rds")
```

