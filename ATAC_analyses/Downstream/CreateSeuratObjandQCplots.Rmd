---
title: "Create Seurat object + QC plots"
output: html_notebook
---

```{r Load libraries}
set.seed(2017)

library(Matrix)
library(tidyverse)
library(chromVAR)
library(motifmatchr)
library(SummarizedExperiment)
library(BiocParallel)
library(TFBSTools)
library(pheatmap)
library(data.table)
library(irlba)
library(Seurat)
library(Signac)
library(harmony)
library(BSgenome.Hsapiens.UCSC.hg38)
library(EnsDb.Hsapiens.v86)
```

## Load consensus peakset

```{r }
# get the couns matrix 
peakfile <- "/g/scb2/zaugg/berest/Projects/collaborations/SCRNAATAC/scratch/CVEJIC/test/snapatac.clust.peaks.bed"

peaks.df = as.data.frame(read_tsv(peakfile, col_names = F))
peaks.df$ID = paste0(peaks.df$X1,":",peaks.df$X2,"-",peaks.df$X3)
rownames(peaks.df) = peaks.df$ID

peaks <- getPeaks(peakfile, sort_peaks = TRUE)

# genome coordinates for binning 
genome <- BSgenome.Hsapiens.UCSC.hg38
seq.gen  = seqlengths(genome)


# external data for TSS enrichment
# create granges object with TSS positions
gene.ranges <- genes(EnsDb.Hsapiens.v86)
gene.ranges <- gene.ranges[gene.ranges$gene_biotype == 'protein_coding', ]

tss.ranges <- GRanges(
  seqnames = seqnames(gene.ranges),
  ranges = IRanges(start = start(gene.ranges), width = 2),
  strand = strand(gene.ranges)
)

seqlevelsStyle(tss.ranges) <- 'UCSC'
tss.ranges <- keepStandardChromosomes(tss.ranges, pruning.mode = 'coarse')


```

## Load metadata
```{r}
# load first metadata
coldata = as.data.frame(read_tsv("/datadisk/Desktop/Projects/SCRNATAC/scATAC/meta.lane234.txt"))
# load statistics from alignment and removing duplicates
stats.df = read_tsv("/datadisk/Desktop/Projects/SCRNATAC/scATAC/stats.lane234.txt")

# merge into on object
coldata.x = merge(coldata, stats.df, by = "ID")
# load experimental metadata
lane.meta = read_csv("/datadisk/Desktop/Projects/SCRNATAC/scATAC/MetaData_scATAC-Seq/Lane234.csv")
# final merging 
coldata.final = merge(coldata.x, lane.meta, by.x = "ID", by.y = "SANGER SAMPLE ID")

rm(coldata,coldata.x,stats.df,lane.meta)
rownames(coldata.final) = paste0(coldata.final$ID,".clean.bam")

# split the column 
coldata.final$origin = str_split(coldata.final$`SUPPLIER SAMPLE NAME`, " ", 4, simplify = TRUE)[,4]


```
## Create Signac object
```{r }
# load or create peak and bin matrices 
fragment.path =  "/g/scb2/zaugg/berest/Projects/collaborations/SCRNAATAC/scratch/CVEJIC/test/merged.l234.sort.CO.bed.gz"


peak_matrix_name = "/g/scb2/zaugg/berest/Projects/collaborations/SCRNAATAC/scratch/CVEJIC/test/merged234.SNAPpeaksnotfilt.Rds"

if (file.exists(peak_matrix_name)) {
    peak_matrix = readRDS(file = peak_matrix_name)
} else {
    peak_matrix <- FeatureMatrix(fragments = fragment.path, features = peaks)
    saveRDS(peak_matrix, file = peak_matrix_name)
  }

bin_matrix_name = "/g/scb2/zaugg/berest/Projects/collaborations/SCRNAATAC/scratch/CVEJIC/test/merged234.SNAPpeaksnotfilt.BM.Rds"

if (file.exists(bin_matrix_name)) {
    bin_matrix = readRDS(file = bin_matrix_name)
} else {
    bin_matrix <- FeatureMatrix(fragments = fragment.path, genome = seq.gen,
                                 binsize = 5000, chunk = 100)
    saveRDS(bin_matrix, file = bin_matrix_name)
  }


# Create object 

merged <- CreateSeuratObject(
  counts = peak_matrix,
  assay = 'peaks',
  project = 'merged234',
  min.cells = 1,
  meta.data = coldata.final
)

# add bins matrix 
merged[["bins"]] = CreateAssayObject(counts = bin_matrix)
# add fragments file to the object 
merged <- SetFragments(
  object = merged,
  file = fragment.path
)

# Binarize bith bins and peaks matrix 
merged = BinarizeCounts(object = merged, assay = c("peaks","bins"))

rm(peak_matrix, bin_matrix)

```

## Quality checks
```{r }
### QC

merged <- FRiP(
  object = merged,
  peak.assay = "peaks",
  bin.assay = "bins"
)

merged@meta.data$blacklist_ratio <- FractionCountsInRegion(
  object = merged,
  assay = 'peaks',
  regions = blacklist_hg38
)


merged <- NucleosomeSignal(object = merged)

merged$nucleosome_group <- ifelse(merged$nucleosome_signal > 10, 'NS > 10', 'NS < 10')
p1 = FragmentHistogram(object = merged)

p1


p2 = VlnPlot(
  object = merged,
  features = c('FRiP', 'blacklist_ratio', 'nucleosome_signal', 'nFeature_peaks'),
  pt.size = 0.1,
  ncol = 4) + NoLegend()

p2 


# to save time use the first 2000 TSSs
merged <- TSSEnrichment(object = merged, tss.positions = tss.ranges[1:2000]) # 

merged$high.tss <- ifelse(merged$TSS.enrichment > 2, 'High', 'Low')
p3 = TSSPlot(merged, group.by = 'high.tss') + ggtitle("TSS enrichment score") + NoLegend()

p3
```
## QC plots for paper
```{r  }
# 
library(ggthemes)
library(viridis)
library(reshape2)
theme_ms <- function(base_size=12, base_family="Helvetica") {
  library(grid)
  (theme_bw(base_size = base_size, base_family = base_family)+
      theme(text=element_text(color="black"),
            axis.title=element_text(face="bold", size = rel(1.3)),
            axis.text=element_text(size = rel(1), color = "black"),
            legend.title=element_text(face="bold"),
            legend.text=element_text(face="bold"),
            legend.background=element_rect(fill="transparent"),
            legend.key.size = unit(0.8, 'lines'),
            panel.border=element_rect(color="black",size=1),
            panel.grid=element_blank()
    ))
}

pFL = ggplot() + geom_histogram(aes( x = p1$data$length, y = (..count..)/sum(..count..)), binwidth = 5, colour = "black", fill = "black") + 
  xlab("Length of the fragments, bp") + ylab("Percent of Fragments") + theme_ms()
pFL
# ggsave("/datadisk/Desktop/Projects/SCRNATAC/scATAC/plots/QC/FL.pdf", plot = pFL, width = 6, height = 3)


pHEX = ggplot(merged@meta.data, aes(x = log10(nFeature_peaks), y = TSS.enrichment)) +
  geom_hex(bins = 100) +
  theme_bw() + scale_fill_viridis() +
  xlab("log10 Fragments in peaks") +
  ylab("TSS Enrichment") +
  ylim(0,10) + 
  geom_hline(yintercept = 2, lty = "dashed") +
  geom_vline(xintercept = log10(1000), lty = "dashed") +
  ggtitle(paste0("Total: ", nrow(merged@meta.data)," Passed: 3611")) + 
  theme_ms()
pHEX
# ggsave("/datadisk/Desktop/Projects/SCRNATAC/scATAC/plots/QC/TSSvsFIP.pdf", plot = pHEX, width = 5, height = 4)

data1 = merged@meta.data[,c("initial")]
data1 = melt(data1)
data1$variable = "initial"
pI = ggplot(data1, aes(x = variable, y = log10(value))) + geom_jitter(alpha = 0.2) + 
  geom_violin(colour = "darkred", fill = "darkred",alpha = 0.7)  +  
  xlab("") + ylab("log10 Initial \n amount of reads") + theme_ms() + 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(),
                      axis.ticks.x=element_blank())

data2 = merged@meta.data[,c("rmDup")]
data2 = melt(data2)
data2$variable = "initial"
pR = ggplot(data2, aes(x = variable, y = log10(value))) + geom_jitter(alpha = 0.2) + 
  geom_violin(colour = "darkred", fill = "darkred",alpha = 0.7)  +  
  xlab("") + ylab("log10 Reads after \n duplicates removal") + theme_ms() + 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(),
                      axis.ticks.x=element_blank())

data3 = merged@meta.data[,c("duplication_rate")]
data3 = melt(data3)
data3$variable = "initial"
pD = ggplot(data3, aes(x = variable, y = value*100)) + geom_jitter(alpha = 0.2) + 
  geom_violin(colour = "darkred", fill = "darkred",alpha = 0.7)  +  
  xlab("") + ylab("Duplication rate, %") + theme_ms() + 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(),
                      axis.ticks.x=element_blank())

data4 = merged@meta.data[,c("nCount_bins")]
data4 = melt(data4)
data4$variable = "initial"
pF = ggplot(data4, aes(x = variable, y = log10(value))) + geom_jitter(alpha = 0.2) + 
  geom_violin(colour = "darkred", fill = "darkred",alpha = 0.7)  +  
  xlab("") + ylab("log10 Fragments per cell")+ theme_ms()  + 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(),
                      axis.ticks.x=element_blank()) 

data5 = merged@meta.data[,c("FRiP")]
data5 = melt(data5)
data5$variable = "initial"
pFr = ggplot(data5, aes(x = variable, y = value * 100)) + geom_jitter(alpha = 0.2) + 
  geom_violin(colour = "darkred", fill = "darkred",alpha = 0.7)  +  
  xlab("") + ylab("Fragments in peaks, %") + theme_ms() + 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(),
                      axis.ticks.x=element_blank())

data6 = merged@meta.data[,c("blacklist_ratio")]
data6 = melt(data6)
data6$variable = "initial"
pB = ggplot(data6, aes(x = variable, y = value * 100)) + geom_jitter(alpha = 0.2) + 
  geom_violin(colour = "darkred", fill = "darkred",alpha = 0.7)  +  
  xlab("") + ylab("Reads in \n blacklisted regions, %") + theme_ms() + 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(),
                      axis.ticks.x=element_blank())

data7 = merged@meta.data[,c("nucleosome_signal")]
data7 = melt(data7)
data7$variable = "initial"
pN = ggplot(data7, aes(x = variable, y = value )) + geom_jitter(alpha = 0.2) + 
  geom_violin(colour = "darkred", fill = "darkred",alpha = 0.7)  +  
  xlab("") + ylab("Nuclesome signal") + theme_ms() + 
  theme(axis.title.x=element_blank(), axis.text.x=element_blank(),
                      axis.ticks.x=element_blank())

plotC = (pI|pR | pD) / (pF | pFr | pB | pN)

plotC

# ggsave("/datadisk/Desktop/Projects/SCRNATAC/scATAC/plots/QC/QCboxes.pdf", plot = plotC, width = 8, height = 6)

rm(data1,data2,data3,data4,data5,data6,data7,
   pI,pR,pD,pF ,pFr ,pB, pN)


data = merged@meta.data[,c("initial","rmDup","duplication_rate","nCount_bins",
                           "FRiP","blacklist_ratio","nucleosome_signal","TSS.enrichment")]

colnames(data) = c("Initial_reads","Reads_after_rmDUP","duplication_rate",
                   "Fragments_cell","FRIP","Reads_blacklist","Nucleosome_signal","TSS_enrichment")

# write_tsv(data, path = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/plots/QC/QCdata.tsv", col_names = T)
data.add = p1$data[,c("chr","start","end","length")]
# write_tsv(data.add, path = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/plots/QC/QCFragmentLengthdistr.tsv", col_names = T)

rm(data, data.add)
```
## Filter merged object
```{r }
merged.filt = subset(merged, subset = nFeature_peaks > 1000 & 
                        # nFeature_peaks < 20000 & 
                        FRiP > 0.5 & 
                        blacklist_ratio < 0.05 & 
                        nucleosome_signal < 10 & 
                        TSS.enrichment > 2)
merged.filt

table(merged.filt$origin)
table(merged.filt$batch)
table(merged.filt$SAMPLE)

saveRDS(merged.filt, file = "/datadisk/Desktop/Projects/SCRNATAC/scATAC/files/merged234.Snap.filt.Rds")

```

