# directory where the processing is going on
dir=""
# get the names of the bam files
variable=$(ls $dir/bam2  | grep "\.bam$" )


for x in $variable
do
  # basename of the bam files
  basenamefile=$(basename "$dir/bam2/$x" .bam)
  # sort specifically for the bamtobed
  samtools sort -n $dir/bam2/$x > $dir/bed/$basenamefile.sort.bam
  # bam to bed
  bedtools bamtobed -bedpe -i $dir/bed/$basenamefile.sort.bam | awk -v var="$x" '{ print $1, $2, $6,var}' |  sort -k1,1 -k2,2n | uniq -c | awk '{OFS="\t"};{print $2,$3,$4,$5,$1}'  > $dir/bed/$basenamefile.bed
  # remove intermediate files
  rm $dir/bed/$basenamefile.sort.bam

done


# concatenate files from each cell
 cat *.bed > ../merged.bed
# get the paired-end reads and transform to fragments
less merged.bed | awk '{OFS="\t"};{if( $3-$2 > 0 && $3-$2 < 1000) print $1,$2+4,$3-5,$4,$5}'  > merged.filt.bed
sort -k1,1 -k2,2n merged.filt.bed > merged.filt.sort.bed
# gzip bed file
bgzip merged.filt.sort.bed
# index it
tabix -p bed merged.filt.sort.bed.gz
